package com.example.launcherapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.launcherapp.AppData
import com.example.launcherapp.MyApplication
import com.example.test2.adapter.MyAdapter
import kotlinx.android.synthetic.main.activity_launcher.*

class LauncherActivity : AppCompatActivity() {
    lateinit var myAdapter: MyAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)


        val lm =
            StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
        recyclerview.setLayoutManager(lm)
        recyclerview.setItemAnimator(DefaultItemAnimator())
        myAdapter=MyAdapter(this)
        recyclerview.setAdapter(myAdapter)

        (application as MyApplication).getAppList().observe(this, object : Observer<List<AppData>>{
            override fun onChanged(t: List<AppData>?) {
               myAdapter.refreshList(t)
            }
        })
        registerForContextMenu(recyclerview);
    }


    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        menuInflater.inflate(R.menu.menu,menu);
    }

}