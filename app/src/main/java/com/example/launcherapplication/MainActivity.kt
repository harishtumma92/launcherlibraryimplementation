package com.example.launcherapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.launcherapp.AppData
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_lunch.setOnClickListener {
            var intent= Intent(MainActivity@this,LauncherActivity::class.java)
            startActivity(intent)
        }
    }
}