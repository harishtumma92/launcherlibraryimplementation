package com.example.test2.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.launcherapp.AppData
import com.example.launcherapp.MyApplication
import com.example.launcherapplication.R
import timber.log.Timber


class MyAdapter(val context: Context):RecyclerView.Adapter<MyAdapter.MyviewHolder>(){

     var list: ArrayList<AppData>?
    init {
        list=ArrayList()
    }

   fun refreshList(t: List<AppData>?)
    {
       list?.clear()
       list?.addAll(t!!)
       this.notifyDataSetChanged()
   }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAdapter.MyviewHolder {
        var view = LayoutInflater.from(parent.getContext()).inflate(
            R.layout.single_layout,
            parent,
            false
        );
        return MyviewHolder(view)
    }

    override fun onBindViewHolder(holder: MyAdapter.MyviewHolder, position: Int) {

        try {
            holder.name?.text = list?.get(position)?.name
            holder.image?.setImageDrawable(list?.get(position)?.icon)
            holder.layout?.setOnClickListener {
                (context.applicationContext as MyApplication).launchApp(list?.get(position)?.pakageName!!)
            }
            holder.layout?.setOnCreateContextMenuListener(object :
                View.OnCreateContextMenuListener {
                override fun onCreateContextMenu(
                    p0: ContextMenu?,
                    p1: View?,
                    p2: ContextMenu.ContextMenuInfo?
                ) {
                    p0?.clear()
                    var item = p0?.add(Menu.NONE, 1, 1, "Unistall")
                    item?.setOnMenuItemClickListener(object : MenuItem.OnMenuItemClickListener {
                        override fun onMenuItemClick(p0: MenuItem?): Boolean {
                            unIstallApp(list?.get(position)?.pakageName!!)
                            //   (context.applicationContext as MyApplication).unIstallApp(list?.get(position)?.pakageName!!)
                           // (context.applicationContext as MyApplication).unIstallApp(list?.get(position)?.pakageName!!)
                            return false
                        }
                    })

                }

            })




        } catch (e: Exception)
        {}
    }


    fun unIstallApp(pakageName: String){
        try {
            Timber.d("uninstall pakage name: $pakageName")
            val intent = Intent(Intent.ACTION_DELETE)
            intent.data = Uri.parse("package:$pakageName")
           // context.startActivity(intent)

          /*  var uri=Uri.parse("package:$pakageName")
            val intent = Intent(
                Intent.ACTION_DELETE, Uri.fromParts(
                    "package",
                   context.packageManager.getPackageArchiveInfo(uri.getPath()!!, 0)?.packageName, null
                )
            )*/
            context.startActivity(intent)
        }
        catch (e: Exception)
        {
            Timber.e(e.toString())
        }
    }

    override fun getItemCount(): Int {
            return list?.size!!
    }

    fun uninstall(groupId: Int)  {
        try {
            (context.applicationContext as MyApplication).unIstallApp(list?.get(groupId)?.pakageName!!)
        }
        catch (e: java.lang.Exception)
        {
            Timber.e(e.toString())
        }

    }

    class MyviewHolder(view: View):RecyclerView.ViewHolder(view) {
        //var mailLayout: LinearLayout? = view.findViewById(R.id.main_layout)
        var image: ImageView? = view.findViewById(R.id.image)
        var name: TextView? = view.findViewById(R.id.txt)
        var layout: LinearLayout? = view.findViewById(R.id.layout)



    }
    interface ContexMenuListner{
        fun onDelete()
    }

}